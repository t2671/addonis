package com.telerikacademy.addonis.services.contracts;

import com.telerikacademy.addonis.models.User;
import org.springframework.mail.SimpleMailMessage;

public interface MailService {

    void sendMail(User recipient, SimpleMailMessage mailMessage);

    void sendMail(User recipient, String subject, String body);

}
