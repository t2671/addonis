package com.telerikacademy.addonis.services.contracts;

import com.telerikacademy.addonis.models.Addon;

public interface RepoInfoService {

    void createInfoForAddon(Addon addon);

    void updateInfoForAddon(Addon addon);

}
